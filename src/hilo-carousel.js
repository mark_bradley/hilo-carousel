//License: MIT
//Date: 11/4/2018
//Author: Mark Bradley
'use strict';

(function () {
    class Carousel extends HTMLElement {
        constructor() {
            // establish prototype chain
            super();

            // attaches shadow tree and returns shadow root reference
            // https://developer.mozilla.org/en-US/docs/Web/API/Element/attachShadow
            const shadow = this.attachShadow({mode: 'open'});

            // creating a container for the hilo-carousel component
            const CarouselContainer = document.createElement('div');

            // get attribute values from getters
            const title = this.title;
            const addItemText = this.addItemText;
            const listItems = this.items;
            //let data = ["home1.jpg", "home2.jpg", "home3.jpg", "home4.jpg", "home5.jpg"];
            let data = ['dest1.jpg', 'dest2.jpg', 'dest3.jpg', 'dest4.jpg'];
            let slides = null;
            let currentSlide = 1;
            let height = (this.height) ? this.height : '100%';
            let width = (this.width) ? this.width : '100%';
            let slideTimer = null;
            let animating = false;


            CarouselContainer.innerHTML = `
        <style>
          hilo-carousel {
            position: relative;
            display: flex;
            
            height:100%;
          }
          hilo-carousel:host {
            position: relative;
            display: flex;
            
            height:100%;
          }
          .hilo-container {
              position : relative;
              display : flex;
              overflow : hidden;
              width : 100%;
              max-width : 100vw;
              height : 100vh;
              margin : 0 auto;
          }
          .hilo-container:hover .position-controls, .hilo-container:hover .bullet-navigation{
              visibility: visible;
           }
          .carousel-item-list {
            z-index: 1;
            position: absolute;
            top: 0px;
            left: 0px;
            
            width: 100%;
            height: 100%;
            display: flex;
          }
          .hidden {
            display: none;
            visibility: hidden;
          }
          .carousel-item-list > img {
            display: block;
            width: 100%;
          }
          .bullet-navigation {
            z-index: 10;
            position: absolute;
            bottom: 5%;
            right: 5%;
            visibility: hidden;
          }
          .bullet-navigation span {
            width: 10px;
            height: 10px;
            padding-right: 10px;
            color: #FFF;
            border-radius: 50%;
            background-color: #FFF;
            -webkit-filter: drop-shadow( 2px 2px 3px #000 ); 
            filter: drop-shadow( -2px 0px 3px #000 );
            cursor: pointer;
          }
          .bullet-navigation .bullet-navigation-active {
            background-color: #10ace7;
          }
          .position-controls {
            z-index: 2;
            position: absolute;
            width:100%;
            top: 50%;
            visibility: hidden;
            
            display: flex;
            justify-content: space-between;
            justify-self: center;
          }
          .hilo-carousel-previous, .hilo-carousel-next {
            height: 37px;
            width: 37px;
            fill: #FFF;
            -webkit-filter: drop-shadow( -2px 0px 3px #000 ); 
            filter: drop-shadow( -2px 0px 3px #000 );
            cursor: pointer;
          }
          .carousel-info {
            display: block;
            z-index: 2;
            position: absolute;;
            top: 5%;
            right: 10%;
            color: #ffffff;
            width: 200px;
            height: auto;
            max-height: 120px;
            
            padding: 20px 20px;
            
            background: rgba(0, 161, 222, 0.65);
            border-radius: 5px;
            
            -webkit-filter: drop-shadow( 5px 5px 3px #000 ); 
            filter: drop-shadow( 5px 5px 3px #000);
          }
          .carousel-info a {
            color: #FFFFFF;
          }
          .carousel-info span {
            text-transform: uppercase;
            font-weight: bold;
          }
          .carousel-info div {
            height: auto;
          }
          
            .animated {
                animation-duration: 1s;
                animation-fill-mode: both;
            }
            
            .animated.infinite {
                animation-iteration-count: infinite;
            }
            
            .animated.delay-1s {
                animation-delay: 1s;
            }
            
            .animated.delay-2s {
                animation-delay: 2s;
            }
            
            .animated.delay-3s {
                animation-delay: 3s;
            }
            
            .animated.delay-4s {
                animation-delay: 4s;
            }
            
            .animated.delay-5s {
                animation-delay: 5s;
            }
            
            .animated.fast {
                animation-duration: 800ms;
            }
            
            .animated.faster {
                animation-duration: 500ms;
            }
            
            .animated.slow {
                animation-duration: 2s;
            }
            
            .animated.slower {
                animation-duration: 3s;
            }
            
            @media (prefers-reduced-motion) {
                .animated {
                    animation: unset !important;
                    transition: none !important;
                }
            }
            
            @keyframes fadeIn {
              from {
                opacity: 0;
              }
            
              to {
                opacity: 1;
              }
            }
            
            .fadeIn {
              animation-name: fadeIn;
            }
                        
            @keyframes slideOutLeft {
                from {
                    transform: translate3d(0, 0, 0);
                }
            
                to {
                    transform: translate3d(-100%, 0, 0);
                }
            }
            
            .slideOutLeft {
                animation-name: slideOutLeft;
            }
            
            
            @keyframes slideOutRight {
              from {
                transform: translate3d(0, 0, 0);
              }
            
              to {
                transform: translate3d(100%, 0, 0);
              }
            }
            
            .slideOutRight {
              animation-name: slideOutRight;
            }

        </style>
        <div class="hilo-container">
            <div class="carousel-item-list hidden">
            </div>
            <div class="bullet-navigation">
            </div>     
            <div class="position-controls">   
                 <duv class="hilo-carousel-previous">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                      <path d="M213.7 256L380.9 81.9c4.2-4.3 4.1-11.4-.2-15.8l-29.9-30.6c-4.3-4.4-11.3-4.5-15.5-.2L131.1 247.9c-2.2 2.2-3.2 5.2-3 8.1-.1 3 .9 5.9 3 8.1l204.2 212.7c4.2 4.3 11.2 4.2 15.5-.2l29.9-30.6c4.3-4.4 4.4-11.5.2-15.8L213.7 256z"/>
                    </svg>
                 </duv>
                 <div class="hilo-carousel-next">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                    <path d="M298.3 256L131.1 81.9c-4.2-4.3-4.1-11.4.2-15.8l29.9-30.6c4.3-4.4 11.3-4.5 15.5-.2L380.9 248c2.2 2.2 3.2 5.2 3 8.1.1 3-.9 5.9-3 8.1L176.7 476.8c-4.2 4.3-11.2 4.2-15.5-.2L131.3 446c-4.3-4.4-4.4-11.5-.2-15.8L298.3 256z"/>
                  </svg>
                </div>
            </div>
            <div class="carousel-info">
            </div>   
        </div>
      `;

            // adding a class to our container for the sake of clarity
            CarouselContainer.classList.add('hilo-carousel');

            // binding methods
            this.rotateLeft = this.rotateLeft.bind(this);
            this.rotateRight = this.rotateRight.bind(this);
            this.rotate = this.rotate.bind(this);
            this.arrangeSlides = this.arrangeSlides.bind(this);
            this.handleNavigationListeners = this.handleNavigationListeners.bind(this);
            this.componentReady = this.componentReady.bind(this);
            this.handleData = this.handleData.bind(this);
            this.adjustMargins = this.adjustMargins.bind(this);
            this.updateInfo = this.updateInfo.bind(this);

            const itemsUrl = this.getAttribute('images') || '';

            this.getData(itemsUrl, this.handleData);
            //this.data = ["images/home1.jpg", "images/home2.jpg", "images/home3.jpg", "images/home4.jpg", "images/home5.jpg"];

            // appending the container to the shadow DOM
            shadow.appendChild(CarouselContainer);

            let container = this.shadowRoot.querySelector('.hilo-container');

            container.style.height = (height) ? `${height}px` : container.style.height;
            container.style.width = (width) ? `${width}px` : container.style.width;
        }

        componentReady() {
            // creating the inner HTML of the editable list element
            let carousel = this.shadowRoot.querySelector('.carousel-item-list');

            //if there are not enough slides duplicate them
            if(this.data.length < 4) {
                this.data = this.data.concat(this.data);
            }

            carousel.innerHTML = `
            ${this.data.map((item, index) => `
                <img src="${item.url}" data-slideid="${index}">
              `).join('')}
            `;

            let navigation = this.shadowRoot.querySelector('.bullet-navigation');

            navigation.innerHTML = `
            ${this.data.map((item, index) => `
                <span class="${(index === 1) ? "bullet-navigation-active" : ""}" data-bulletID="${index}">&nbsp;&nbsp;</span>
              `).join('')}
            `;

            let info = this.shadowRoot.querySelector('.carousel-info');

            info.innerHTML = `
            ${this.data.map((item, index) => `
                <div data-infoid="${index}" class=${(index !== 0) ? "hidden" : ""}>
                    <span>${item.title}</span>
                    <p>${item.body}</p>
                    <a href="${item.link}" target="_blank">${item.linktext}</a>
                </div>
              `).join('')}
            `;

            //wait for image to load and adjust margins
            let imageSize = carousel.querySelector('img');
            imageSize.addEventListener('load', this.adjustMargins);

            carousel.prepend(carousel.children[carousel.children.length - 1]);

            this.slides = carousel.querySelectorAll('img');

            if (this.auto === "true") {
                this.slideTimer = setInterval(this.rotateRight, 5000);
            }

            this.updateBulletNavigation();
        }

        // fires after the element has been attached to the DOM
        connectedCallback() {
            const rotateLeftButton = this.shadowRoot.querySelector('.hilo-carousel-previous');
            const rotateRightButton = this.shadowRoot.querySelector('.hilo-carousel-next');
            const navigation = this.shadowRoot.querySelector('.bullet-navigation');

            this.itemList = this.shadowRoot.querySelector('.carousel-item-list');

            rotateLeftButton.addEventListener('click', this.rotateLeft, false);
            rotateRightButton.addEventListener('click', this.rotateRight, false);
            navigation.addEventListener('click', this.handleNavigationListeners, false);
        }

        adjustMargins(e){
            let carouselImagesContainer = this.shadowRoot.querySelector('.carousel-item-list');
            let carouselContainer = this.shadowRoot.querySelector('.hilo-container');

            // if window width is greater than image width then return the remainder and divide it by two to give space for surrounding images
            if(document.body.scrollWidth >= e.target.width && !this.width) {
                carouselImagesContainer.style.marginLeft = `-${e.target.width - ((document.body.scrollWidth % e.target.width) / 2)}px`;
            }else{
                carouselImagesContainer.style.marginLeft = `-${e.target.width}px`
            }
            carouselContainer.style.height = `${e.target.height}px`;
            carouselImagesContainer.classList.add('animated','fadeIn');
            carouselImagesContainer.classList.remove('hidden');
        }

        //to do: refactor right and left into single rotate function
        rotateRight(e) {
            this.rotate(e, 'slideOutLeft', 'left');
        }

        rotateLeft(e) {
            this.rotate(e, 'slideOutRight', 'right');
        }

        rotate(e, animationClass, direction){
            //if still animating reject click
            if(this.animating){
                return false;
            }
            //if user clicks an arrow stop automatic sliding
            if (e) {
                this.clearInterval(this.slideTimer);
            }

            this.slides.forEach((slide) => {
                slide.classList.add('animated', animationClass);
            });

            this.animating = true;

            setTimeout(this.arrangeSlides, 1000, direction);
        }

        //clear a repeating timer, example auto slide
        clearInterval(timer){
            window.clearInterval(timer);
        }

        //arrange slides before next rotation so that there isn't a blank space
        arrangeSlides(direction) {
            this.animating = false;

            let carousel = this.shadowRoot.querySelector('.carousel-item-list');

            if (direction === 'left') {
                carousel.appendChild(carousel.children[0]);
            } else {
                carousel.prepend(carousel.children[carousel.children.length - 1]);
            }

            //clear animation classes
            this.slides.forEach((slide, index) => {
                slide.className = '';
            });

            this.updateInfo();
            this.updateBulletNavigation();
        }

        handleNavigationListeners(e) {
            let slideNumber = e.target.getAttribute('data-bulletid');

            this.clearInterval(this.slideTimer);
            this.goToSlide(slideNumber);
        }

        goToSlide(slideNumber){
            let currentSlide = this.shadowRoot.querySelector('.carousel-item-list').children[1].getAttribute("data-slideid");
            let carousel = this.shadowRoot.querySelector('.carousel-item-list');
            let newSlideOrder = Array.from(carousel.children);

            if(currentSlide === slideNumber){
                return false;
            }

            newSlideOrder.sort((a, b) => {
                return a.getAttribute("data-slideid") - b.getAttribute("data-slideid")
            }
            );

            newSlideOrder.push.apply(newSlideOrder, newSlideOrder.splice(0, slideNumber));
            newSlideOrder.unshift(newSlideOrder.pop());

            carousel.innerHTML = "";

            newSlideOrder.forEach((el) => {
                carousel.appendChild(el);
            });

            this.updateInfo();
            this.updateBulletNavigation();

            this.slides = carousel.querySelectorAll('img');
        }

        updateInfo(){
            let currentSlide = parseInt(this.shadowRoot.querySelector('.carousel-item-list').children[1].getAttribute("data-slideid"));
            let info = Array.from(this.shadowRoot.querySelector('.carousel-info').children);

            info.map((item, index) => {
                if(currentSlide === index) {
                    item.classList.remove('hidden');
                    item.classList.add('animated', 'fadeIn');
                }else{
                    item.classList.add('hidden');
                }
            });
        }

        updateBulletNavigation(){
            let currentSlide = parseInt(this.shadowRoot.querySelector('.carousel-item-list').children[1].getAttribute("data-slideid"));
            let bulletNavigation = Array.from(this.shadowRoot.querySelector('.bullet-navigation').children);

            bulletNavigation.map((item, index) => {
                if(currentSlide === index) {
                    item.classList.add('bullet-navigation-active');
                }else{
                    item.classList.remove('bullet-navigation-active');
                }
            });
        }

        // gathering data from element attributes
        get title() {
            return this.getAttribute('title') || '';
        }

        get height() {
            return this.getAttribute('height');
        }

        get width() {
            return this.getAttribute('width');
        }

        get auto() {
            return (this.getAttribute('auto')) ? this.getAttribute('auto') : "true";
        }

        handleData(data){
            console.log(data);
            this.data = data.slides;

            this.componentReady();
        }

        //to do: async fetch
        async getData(Url, callback) {
            var xhr = new XMLHttpRequest();

            xhr.responseType = 'json';
            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4) {
                    callback(xhr.response);
                }
            }

            xhr.open('GET', Url, true);
            xhr.send('');
        }
    }


    // let the browser know about the custom element
    customElements.define('hilo-carousel', Carousel);
})();
