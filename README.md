# README #
Author: mark.obradley@gmail.com

## Instructions to run demo ##

1. In the root folder, run "npm install"
2. Run "npm run serve"
3. Go to http://127.0.0.1:9999/index.html in Chrome / Firefox
4. Also go to http://127.0.0.1:9999/KLMdemo.html to see it embedded in a page

## Reasoning behind development ##

I choose to build the carousel using the Web Component API provided in the latest browsers. This will allow the component to be imported
using a script tag and then added to a page using <hilo-carousel> element tag. Eventually component development will shift toward
native Web Components. Even frameworks like Angular and Stencil.js are aiming to compile to Universal Web Components. Once tooling makes
web component development efficient and support becomes standard across browsers then it will be much more easy to maintain a set of
universal components. 

The component takes a URL passed to the images property, which then loads a json configuration, which has a format like this:

```
{
  "slides": [
    {"url":"images/home1.jpg", "title": "Title One", "body":"Some catchy slogan to inspire you to fly", "link":"http://www.klm.com", "linktext":"Click here"},
    {"url":"images/home2.jpg", "title": "Title Two", "body":"Another catchy slogan to inspire you to fly", "link":"http://www.klm.com", "linktext":"Jump to flights"},
    {"url":"images/home3.jpg", "title": "Title Three", "body":"Furthermore, a catchy slogan to inspire you to fly", "link":"http://www.klm.com", "linktext":"Go to deals"},
    {"url":"images/home4.jpg", "title": "Title Four", "body":"Also, a catchy slogan to inspire you to fly", "link":"http://www.klm.com", "linktext":"See more"},
    {"url":"images/home5.jpg", "title": "Title Five", "body":"Finally, a catchy slogan to inspire you to fly", "link":"http://www.klm.com", "linktext":"Learn more"}
  ]
}
```

Note: I did not use any polyfills in this project, so older browsers like IE 11 are not be supported.

### Some configuration options are: ###
* height="400" >  specify the height of the carousel in pixels
* width="800" >  specify the width of the carousel in pixels
* data="http://someURL.com/data.json" > load slide data from json file at specified URL
* auto="false" > disable automatic scrolling

### TO DO
* fix aspect ratio for custom height / width
* add more property configurations, such as control colors
* refactor unnecessarily referencing the DOM

## Assignment ##

Create one of the carousels as shown on the design (the png file).

### Steps ###

1. Clone this repo.
2. The main instructions are specified in the attached design, make sure you read them!

### Plz also take into account ###

* Please spend three to max four hours on this assignment.
* Do not use any existing libraries to implement the slider (e.g jQuery). Implement it yourself :-).
* Your result does not have to be 100% the same.
* Make it responsive and asynchronous.
* What is important is the motivation of your choices: tell us why you implemented the carousel in a particular way. And for instance why your solution is better compared to another solutions (and off course explain why).

### Bonus ###
1. Bonus: if you add some testing.
2. Bonus: if you can create a second version without JavaScript.
